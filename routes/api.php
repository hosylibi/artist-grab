<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::namespace('Api')->prefix('v1')->group(function () {
    Route::prefix('user')->group(function () {
        Route::post('login-social', 'Auth\UserController@loginSocial');
        Route::post('login', 'Auth\UserController@login');

        Route::group([
            'middleware' => 'auth:api',
            \App\Http\Middleware\CheckOnline::class
        ], function() {
            Route::get('logout', 'Auth\UserController@logout');
            Route::post('update-profile', 'Auth\UserController@updateProfile');
            Route::get('information', 'UserController@index');
        });
    });

});