<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('system_code')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        //

        DB::table('system_code')->insert([
            ['column_name' => 'music_type', 'type' => 1],
            ['column_name' => 'order_status', 'type' => 2],
        ]);
    }
}
