<?php

use Illuminate\Database\Seeder;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        //

        DB::table('categories')->insert([
            ['category' => 'Ca sỹ'],
            ['category' => 'Nghệ sĩ Piano'],
            ['category' => 'Nghệ sĩ Trống'],
            ['category' => 'Nghệ sĩ Guitar'],
            ['category' => 'Nghệ sĩ Sáo'],
        ]);
    }
}
