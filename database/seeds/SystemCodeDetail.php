<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemCodeDetail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('system_code_detail')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        //
        DB::table('system_code_detail')->insert([
            ['system_code_id' => '1','column_name' => 'music_type_pop', 'code' => 1, 'value' => 'Pop'],
            ['system_code_id' => '1','column_name' => 'music_type_ballad', 'code' => 2, 'value' => 'Ballad'],
            ['system_code_id' => '1','column_name' => 'music_type_r_and_b', 'code' => 3, 'value' => 'R&B'],
            ['system_code_id' => '2','column_name' => 'order_status_waiting', 'code' => 1, 'value' => 'Chưa chấp nhận'],
            ['system_code_id' => '2','column_name' => 'order_status_accepted', 'code' => 2, 'value' => 'Đã chấp nhận'],
            ['system_code_id' => '2','column_name' => 'order_status_purchaged', 'code' => 3, 'value' => 'Đã thanh toán'],
            ['system_code_id' => '2','column_name' => 'order_status_caneled', 'code' => 4, 'value' => 'Đã từ chối'],
        ]);
    }
}
