<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist_name')->nullable();
            $table->string('stage_name')->nullable();
            $table->string('sex')->nullable();
            $table->date('birthday')->nullable();
            $table->string('home_town')->nullable();
            $table->string('job')->nullable();
            $table->integer('artist_type')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('facebook')->nullable();
            $table->longText('description')->nullable();
            $table->string('workplace')->nullable();
            $table->float('internal_price')->default(0);
            $table->float('foreign_price')->default(0);
            $table->float('holiday_price')->default(0);
            $table->float('other_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
