<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemCodeDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('system_code_detail');

        Schema::create('system_code_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('system_code_id');
            $table->string('column_name');
            $table->integer('code');
            $table->string('value');
            $table->timestamps();
            $table->foreign('system_code_id')->references('id')->on('system_code')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_code_detail');
    }
}
