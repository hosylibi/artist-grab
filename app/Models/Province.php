<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Province
 *
 * @property int $id
 * @property string|null $type
 * @property string $province
 * @property int $priority
 */
class Province extends Model
{
    //
    protected $table = 'provinces';

    protected $fillable = [
        'type',
        'province',
        'priority',
    ];

    public $timestamps = false;

    public function districts()
    {
        return $this->hasMany(District::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
