<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Artist
 *
 * @property int $id
 * @property int user_id
 * @property string|null $artist_name
 * @property string|null $stage_name
 * @property string|null $sex
 * @property string|null $birthday
 * @property string|null $home_town
 * @property string|null $job
 * @property int|null $artist_type
 * @property string|null $phone
 * @property string|null $facebook
 * @property string|null $youtube
 * @property string|null $description
 * @property string|null $workplace
 * @property float $internal_price
 * @property float $foreign_price
 * @property float $holiday_price
 * @property float $other_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class Artist extends Model
{
    //
    protected $table = 'artists';

    protected $casts = [];

    protected $date = [
        'birthday:Y-m-d',
    ];

    protected $hidden = [];

    protected $fillable = [
        'user_id',
        'artist_name',
        'stage_name',
        'sex',
        'birthday',
        'home_town',
        'job',
        'artist_type',
        'phone',
        'facebook',
        'youtube',
        'description',
        'workplace',
        'internal_price',
        'foreign_price',
        'holiday_price',
        'other_price',
    ];
}
