<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property int $customer_type
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $password
 * @property string $email
 * @property string|null $birthday
 * @property string|null $address
 * @property string|null $google_id
 * @property string|null $facebook_id
 * @property string|null $avatar
 * @property Carbon|null $last_logged_in_at
 * @property Carbon|null $last_online_at
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property int $is_active
 * @property int $is_verified
 * @property int|null $referrer_id
 * @property int $is_admin
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    const LOGIN_WITH_CUSTOMER = 1;
    const LOGIN_WITH_ARTIST = 2;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_type',
        'name',
        'phone',
        'password',
        'email',
        'birthday',
        'address',
        'google_id',
        'facebook_id',
        'avatar',
        'last_logged_in_at',
        'last_online_at',
        'email_verified_at',
        'is_active',
        'is_verified',
        'referrer_id',
        'is_admin',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'last_logged_in_at',
        'last_online_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_logged_in_at' => 'datetime',
        'last_online_at'    => 'datetime',
    ];

    public function artist()
    {
        return $this->hasOne(Artist::class,'user_id');
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar) {
            if (strpos($this->avatar, "http://") !== false) {
                return $this->avatar;
            }
            if ($this->avatar && Storage::exists('users/avatars/' . $this->avatar))
                return asset(Storage::url('users/avatars/' . $this->avatar));
        }
        return '';
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }

    public function getIsOnlineAttribute()
    {
        if ($this->last_online_at && $this->last_online_at->diffInHours(now()) === 0)
            return true;
        return false;
    }

    public function getTotalOnlineMembers()
    {
        return $this->members()->where('last_online_at', '>=', now()->subHour())->count();
    }

    public function getTotalOfflineMembers()
    {
        return $this->members()->where('last_online_at', '<', now()->subHour())->count();
    }

    public function device_tokens()
    {
        return $this->hasMany(DeviceToken::class);
    }

    public function role()
    {
        return $this->hasOne(UserRole::class)
            ->where('is_active', 1)
            ->where(function ($q) {
                $q->whereNull('end_date')
                    ->orWhere('end_date', '>=', Carbon::today());
            });
    }
}
