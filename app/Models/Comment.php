<?php

namespace App\Models;

use App\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Comment
 *
 * @property int $id
 * @property int $post_id
 * @property string $email
 * @property string $name
 * @property string $content
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Post $post
 *
 * @package App\Models
 */
class Comment extends Model
{
    //
    protected $table = 'comments';

    protected $casts = [
        'post_id' => 'int'
    ];

    protected $fillable = [
        'post_id',
        'email',
        'name',
        'content',
        'user_id'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class,'parent_id');
    }
}
