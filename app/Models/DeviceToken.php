<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{
    //
    protected $table = 'device_tokens';

    protected $casts = [
        'user_id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'mac_address',
        'device_token'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
