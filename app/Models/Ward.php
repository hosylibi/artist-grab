<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ward
 *
 * @property int $id
 * @property int $district_id
 * @property string|null $type
 * @property string $ward
 */
class Ward extends Model
{
    //
    protected $table = 'wards';

    protected $casts = [
        'district_id' => 'int'
    ];

    protected $fillable = [
        'district_id',
        'type',
        'ward',
    ];

    public $timestamps = false;

    public function district()
    {
        return $this->belongsTo(District::class);
    }
}
