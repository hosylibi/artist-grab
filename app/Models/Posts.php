<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Posts extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'post_category_id',
        'title',
        'short_description',
        'description',
        'type',
        'is_active'
    ];
}
