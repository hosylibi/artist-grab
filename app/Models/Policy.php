<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Policy
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $policy
 * @property int $is_active
 * @property int $is_highlight
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class Policy extends Model
{
    //
    protected $table = 'policies';

    protected $casts = [
        'is_active'    => 'bool',
        'is_highlight' => 'bool',
        'sort'         => 'int'
    ];

    protected $fillable = [
        'name',
        'image',
        'policy',
        'is_active',
        'is_highlight',
        'sort',
    ];
}
