<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderBooking
 *
 * @property int $id
 * @property int $user_id
 * @property int $artist_id
 * @property string $location
 * @property string $address
 * @property string $start_date
 * @property string $end_date
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class OrderBooking extends Model
{
    //
    protected $table = 'order_bookings';

    protected $fillable = [
        'user_id',
        'artist_id',
        'location',
        'address',
        'start_date',
        'end_date',
        'status',
    ];
}
