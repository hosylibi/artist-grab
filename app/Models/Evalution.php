<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evalution extends Model
{
    //
    protected $table = 'evalutions';

    protected $casts = [
        'post_id' => 'int'
    ];

    protected $fillable = [
        'post_id',
        'email',
        'name',
        'content',
        'user_id'
    ];
}
