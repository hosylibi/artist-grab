<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\District
 *
 * @property int $id
 * @property int $province_id
 * @property string|null $type
 * @property string $district
 */
class District extends Model
{
    //
    protected $table = 'districts';

    protected $casts = [
        'province_id' => 'int'
    ];

    protected $fillable = [
        'province_id',
        'type',
        'district',
    ];

    protected $with = [
        'province',
    ];

    public $timestamps = false;

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function wards()
    {
        return $this->hasMany(Ward::class);
    }
}
