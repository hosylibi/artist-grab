<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Object_;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->is('api/*')) {
            return [
                'name'     => 'required|string|max:50',
                'phone'    => 'required|string|vietnam_phone|unique:users,phone,NULL,id,deleted_at,NULL',
                'password' => 'required|string|min:6',
                'birthday' => 'nullable|date|date_format:Y-m-d',
                'email'    => 'nullable|email|string|unique:users,email',
                'address'  => 'nullable|string',
                'avatar'   => 'nullable|image|mimes:jpeg,png,jpg,svg|max:5120',

            ];

        } else {
            return [
                'name'     => 'required|string|max:50',
                'phone'    => 'required|string|regex:/^[0-9]*$/i|size:10|unique:users',
                'password' => 'required|string|min:6',
                'email'    => 'email|unique:users|regex:/^[@A-Za-z0-9._-]*$/i',
            ];
        }
    }

    public function messages()
    {
        return [
            'required'            => 'Vui lòng nhập :attribute.',
            'integer'             => 'Vui lòng nhập đúng :attribute.',
            'string'              => 'Vui lòng nhập đúng :attribute.',
            'boolean'             => 'Vui lòng nhập đúng :attribute.',
            'name.max'            => 'Vui lòng nhập :attribute tối đa :max kí tự',
            'min'                 => 'Vui lòng nhập :attribute tối thiểu :min kí tự',
            'phone.size'          => 'Vui lòng nhập đúng :attribute.',
            'phone.vietnam_phone' => 'Vui lòng nhập đúng :attribute.',
            'unique'              => ':attribute đã được sử dụng.',
            'exists'              => ':attribute không tồn tại.',
            'image'               => 'Vui lòng chọn đúng :attribute',
            'mimes'               => 'Vui lòng chọn :attribute với định dạng: jpeg, png, jpg, svg',
            'max'                 => 'Vui lòng chọn :attribute có kích thước <= 5MB',
            'regex'               => 'Vui lòng nhập đúng :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'name'                => 'Họ tên',
            'phone'               => 'Số điện thoại',
            'password'            => 'Mật khẩu',
            'birthday'            => 'Ngày sinh',
            'email'               => 'Email',
            'address'             => 'Địa chỉ',
            'role_id'             => 'Loại thành viên',
            'province_id'         => 'Tỉnh/Thành phố hoạt động',
            'district_id'         => 'Quận/Huyện hoạt động',
            'manager_phone'       => 'Người quản lý',
            'referer_phone'       => 'Người giới thiệu',
            'avatar'              => 'Ảnh đại diện',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if (request()->is('api/*')) {
            throw new HttpResponseException(
                response()->json([
                    'result'  => false,
                    'message' => $validator->errors()->first(),
                    'data'    => new Object_()
                ], Response::HTTP_BAD_REQUEST)
            );
        }
        return parent::failedValidation($validator);
    }
}
