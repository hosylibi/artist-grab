<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Object_;

class LoginSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'facebook_id' => 'nullable|required_without:google_id|string',
            'google_id'   => 'nullable|required_without:facebook_id|string',
            'email'       => 'nullable|email',
        ];
    }

    public function messages()
    {
        return [
            'required'         => 'Vui lòng nhập :attribute.',
            'required_without' => 'Vui lòng nhập facebook_id hoặc google_id.',
            'email'            => 'Vui lòng nhập đúng :attribute.',
            'string'           => 'Vui lòng nhập đúng :attribute.',
        ];
    }

    public function attributes()
    {
        return [
            'email'       => 'email',
            'facebook_id' => 'facebook_id',
            'google_id'   => 'google_id',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if (request()->is('api/*')) {
            throw new HttpResponseException(
                response()->json([
                    'result'  => false,
                    'message' => $validator->errors()->first(),
                    'data'    => new Object_()
                ], Response::HTTP_BAD_REQUEST)
            );
        }
        return parent::failedValidation($validator);
    }
}
