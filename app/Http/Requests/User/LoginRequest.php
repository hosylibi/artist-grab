<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Object_;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->is('api/*')) {
            return [
                'phone'    => 'required|string|exists:users,phone',
                'password' => 'required|string|min:6'
            ];
        } else {
            return [
                'phone'    => 'required|string|exists:users,phone',
                'password' => 'required|string|min:6'
            ];
        }
    }

    public function messages()
    {
        return [
            'required'            => 'Vui lòng nhập :attribute.',
            'min'                 => 'Vui lòng nhập :attribute tối thiểu :min kí tự',
//            'phone.vietnam_phone' => 'Vui lòng nhập đúng :attribute.',
            'string'              => 'Vui lòng nhập đúng :attribute.',
            'exists'              => ':attribute không tồn tại.',
        ];
    }

    public function attributes()
    {
        return [
            'phone'    => 'Điện thoại',
            'password' => 'Mật khẩu',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if (request()->is('api/*')) {
            throw new HttpResponseException(
                response()->json([
                    'result'  => false,
                    'message' => $validator->errors()->first(),
                    'data'    => new Object_()
                ], Response::HTTP_BAD_REQUEST)
            );
        }
        return parent::failedValidation($validator);
    }
}
