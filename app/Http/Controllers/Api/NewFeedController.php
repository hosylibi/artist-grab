<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NewFeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 10;
        $query    = Posts::query();

        if ($request->key_word) {
            $query->where('title', 'LIKE', "%{$request->key_word}%");
        }

        if ($request->post_category_id) {
            $query->where('post_category_id', $request->post_category_id);
        }

        if ($request->user_id) {
            $query->where('user_id', $request->user_id);
        }

        $data = $query->orderByDesc('created_at')
            ->select('*')
            ->paginate($per_page);
        return $this->success($data, "Danh sach bai post");
    }
}
