<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\LoginSocialRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Models\Artist;
use App\Models\Category;
use App\Models\DeviceToken;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Object_;

class UserController extends Controller
{
    public function register(RegisterRequest $request) {
        try {
            $user                    = new User();
            $user->phone             = $request->phone;
            $user->password          = bcrypt($request->password);
            $user->name              = $request->name;
            $user->address           = $request->address;
            $user->last_logged_in_at = now();
            $user->last_online_at    = now();
            $user->is_active         = 1;
            $user->is_verified       = $request->is_verified;

            if($user->save()) {
                if ($request->device_token && $request->mac_address) {
                    DeviceToken::create([
                        'user_id'      => $user->id,
                        'mac_address'  => $request->mac_address,
                        'device_token' => $request->device_token,
                    ]);
                }

                DB::commit();
                $tokenResult = $user->createToken(env('APP_NAME'));
                $user->auth = $tokenResult;
                $user->token_type = 'Bearer';
                $user->expires_at = Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString();

                return $this->success($user, "Đăng ký thành công!");
            }
        } catch (\Exception $e) {
            return $this->error(new Object_(), $e);
        }
    }

    public function login(LoginRequest $request) {
        try {
            $user = User::where('phone', $request->phone)->first();

            if (!$user) {
                throw new \Exception('Tài khoản hoặc mật khẩu không đúng', Response::HTTP_BAD_REQUEST);
            }

            if (!\Hash::check($request->password, $user->password)) {
                throw new \Exception('Mật khẩu không đúng', Response::HTTP_BAD_REQUEST);
            }

            if (!$user->is_active) {
                throw new \Exception('Tài khoản đã bị khóa. Vui lòng liên hệ quản trị viên.', Response::HTTP_UNAUTHORIZED);
            }

            if (!$user->is_verified) {
                throw new \Exception('Tài khoản chưa được kích hoạt', Response::HTTP_UNAUTHORIZED);
            }

            DB::beginTransaction();
            if ($request->device_token && $request->mac_address) {
                DeviceToken::updateOrCreate([
                    'user_id'     => $user->id,
                    'mac_address' => $request->mac_address,
                ], [
                    'device_token' => $request->device_token
                ]);
            }

            if (!$user->avatar) {
                $user->avatar = $request->avatar;
            }

            $user->last_logged_in_at = now();
            $user->last_online_at    = now();

            if ($user->save()) {
                DB::commit();
                $tokenResult = $user->createToken(env('APP_NAME'));
                $user->auth = $tokenResult;
                $user->token_type = 'Bearer';
                $user->expires_at = Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString();

                return $this->success($user, "Đăng nhập thành công!");
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error(new Object_(), $e);
        }
    }

    //
    public function loginSocial(LoginSocialRequest $request)
    {
        /**
         * @var User $user
         */
        DB::beginTransaction();

        try {
            $facebook_id = filter_var($request->facebook_id, FILTER_SANITIZE_NUMBER_INT);
            $google_id   = filter_var($request->google_id, FILTER_SANITIZE_NUMBER_INT);

            if ($facebook_id) {
                $user = User::firstOrNew(['facebook_id' => $facebook_id]);
            } else {
                $user = User::firstOrNew(['google_id' => $google_id]);
            }

            if ($user->exists && $user->is_verified && !$user->is_active) {
                throw new \Exception('Tài khoản đã bị khóa. Vui lòng liên hệ quản trị viên.', Response::HTTP_UNAUTHORIZED);
            }

            if ($request->email && !$user->email && !User::where('email', $request->email)->exists()) {
                $user->email = $request->email;
            }

            if ($request->phone && !$user->phone && !User::where('phone', $request->phone)->exists()) {
                $user->email = $request->email;
            }

            if ($request->name && !$user->name) {
                $user->name = $request->name;
            }

            $old_avatar = $user->avatar;

            if ($request->hasFile('avatar')) {
                $avatar       = date('Y_m_d_H_i_s') . '-' . uniqid() . "." . strtolower($request->file('avatar')->getClientOriginalExtension());
                $user->avatar = $avatar;
            }

            $user->last_logged_in_at = now();
            $user->last_online_at    = now();
            $user->customer_type = $request->customer_type;

            // Save to database
            if ($user->save()) {
                // Upload Avatar
                if ($request->hasFile('avatar')) {
                    if (Storage::exists('users/avatars/' . $old_avatar)) {
                        Storage::delete('users/avatars/' . $old_avatar);
                    }

                    Storage::putFileAs('users/avatars', $request->file('avatar'), $avatar);
                }

                // Device Token
                if ($request->device_token && $request->mac_address) {
                    DeviceToken::updateOrCreate([
                        'user_id'     => $user->id,
                        'mac_address' => $request->mac_address,
                    ], [
                        'device_token' => $request->device_token
                    ]);
                }

                DB::commit();
                $tokenResult = $user->createToken(env('APP_NAME'));
                $user->auth = $tokenResult;
                $user->token_type = 'Bearer';
                $user->expires_at = Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString();

                return $this->success($user, "Đăng nhập thành công!");
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error(new Object_(), $e);
        }
    }

    public function updateProfile(Request $request) {
        $user = $request->user();

        if(!$user) {
            return response()->json([
                'result'  => false,
                'message' => 'Tài khoản không tồn tại!',
            ], Response::HTTP_NOT_FOUND);
        }

        if($user->customer_type == User::LOGIN_WITH_ARTIST
            || $request->customer_type == User::LOGIN_WITH_ARTIST
        ) {
            $artist = new Artist();

            $job = Category::find($request->category);
            $artist_type = implode(',', $request->artist_type);

            $artist->user_id = $user->id;
            $artist->artist_name = $request->artist_name;
            $artist->stage_name = $request->stage_name; // nghệ danh
            $artist->sex = $request->sex;
            $artist->birthday = $request->birthday;
            $artist->home_town = $request->home_town; // quê quán
            $artist->job = $job->category; // nghề nghiệp
            $artist->artist_type = $artist_type; // nghệ sĩ: trống, piano...
            $artist->phone = $request->phone;
            $artist->facebook = $request->facebook;
            $artist->youtube = $request->youtube;
            $artist->description = $request->description; //thành tựu
            $artist->workplace = $request->workplace; // nơi làm việc
            $artist->internal_price = intval($request->internal_price);
            $artist->foreign_price = intval($request->foreign_price);
            $artist->holiday_price = intval($request->holiday_price);
            $artist->other_price = intval($request->orther_price);

            $artist->save();
        }

        if(!$user->is_active) {
            $user->is_active = 1;
            $user->is_verified = 1;
        }

        $user->phone = isset($request->phone) ? $request->phone : $user->phone;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->address = $request->address;

        if($user->update(
            $request->except(
                'phone',
                'password',
                'name',
                'address',
                'is_active',
                'is_verified'
            )
        )) {
            return $this->success($user, "Cập nhật thành công!");
        }
    }

    public function logout(Request $request)
    {
        try {
            $user = $request->user();

            if($user) {
                DeviceToken::where('user_id', $user->id)->delete();

                $result = $user->token()->delete();

                return $this->success($result, "Đăng xuất thành công!");
            }

            throw new \Exception('Đăng xuất thất bại!', Response::HTTP_UNAUTHORIZED);

        } catch (\Exception $e) {
            return $this->error(new Object_(), $e);
        }
    }
}
