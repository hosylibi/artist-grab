<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use phpDocumentor\Reflection\Types\Object_;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data, $message, $options = [])
    {
        $response = [
            'result' => true,
            'message' => $message,
            'data' => $data
        ];
        if (count($options)) {
            $response = array_merge($response, $options);
        }
        return response()->json($response);
    }

    public function error($data, \Exception $e, $options = [])
    {
        $response = [
            'result' => false,
            'data' => $data,
            'message' => $e->getMessage(),
            'line' => $e->getLine(),
            'file' => $e->getFile(),
        ];
        if (count($options)) {
            $response = array_merge($response, $options);
        }
        return response()->json($response, $e->getCode() && is_numeric($e->getCode()) && $e->getCode() < 500 ? $e->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function validationError($data = Object_::class, $validators = [])
    {
        $errors = array();
        foreach ($validators as $key => $validator) {
            $errors[$key] = $validator[0];
        }
        return response()->json([
            'result' => false,
            'message' => "Vui lòng kiểm tra lại dữ liệu",
            'data' => $data,
            'errors' => $errors
        ], Response::HTTP_BAD_REQUEST);
    }
}
